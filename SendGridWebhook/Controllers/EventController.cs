﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SendGridWebhook.Models;
using NLog;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using SendGridWebhook.Utils;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace SendGridWebhook.Controllers
{
    public class EventController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string IgnoreClient = System.Configuration.ConfigurationManager.AppSettings["IgnoreClient"].ToString();

        [HttpPost]
        public async Task<HttpResponseMessage> Post()
        {
            if (!IsJsonContent(Request.Content))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                MemoryStream target = new MemoryStream();
                using (var stream = await Request.Content.ReadAsStreamAsync())
                {
                    await stream.CopyToAsync(target, 4096).ContinueWith(t => ProcessIncomingEvent(target));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        private void ProcessIncomingEvent(MemoryStream target)
        {
            try
            {
                if (target != null && target.Length > 0)
                {
                    string strTemp = Encoding.UTF8.GetString(target.ToArray());
                    logger.Info(strTemp);
                    JArray ja = (JArray)JsonConvert.DeserializeObject(strTemp);
                    for (int i = 0; i < ja.Count; i++)
                    {
                        var itemProperties = ja[i].Children<JProperty>();
                        //string custormId = ja[i]["custormId"].ToString();
                        //string emailState = ja[i]["event"].ToString().ToLower();
                        //string sendUserID = ja[i]["sendUserID"].ToString();
                        //string eventID = ja[i]["eventID"].ToString();
                        //string timestamp = Common.ConvertUnixTimestamp(ja[i]["timestamp"].ToString());
                        string custormId = "";
                        string emailState = "";
                        string sendUserID = "";
                        string eventID = "";
                        string timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        var csid = itemProperties.FirstOrDefault(x => x.Name == "custormId");
                        if (csid != null)
                            custormId = csid.Value.ToString();
                        var eventType = itemProperties.FirstOrDefault(x => x.Name == "event");
                        if (eventType != null)
                            emailState = eventType.Value.ToString();
                        var client = itemProperties.FirstOrDefault(x => x.Name == "sendUserID");
                        if (client != null)
                            sendUserID = client.Value.ToString();
                        var eventid = itemProperties.FirstOrDefault(x => x.Name == "eventID");
                        if (eventid != null)
                            eventID = eventid.Value.ToString();
                        var ts = itemProperties.FirstOrDefault(x => x.Name == "timestamp");
                        if (ts != null)
                            timestamp = Common.ConvertUnixTimestamp(ts.Value.ToString());

                        string triggerType = null;
                        string clickURL = "";
                        int actionTypeID = 0;
                        if (emailState == "bounce")
                        {
                            triggerType = "EmailBounce";
                            actionTypeID = 18;
                        }
                        else if (emailState == "open")
                        {
                            triggerType = "EmailOpen";
                            actionTypeID = 12;
                        }
                        else if (emailState == "spamreport")
                        {
                            triggerType = "EmailSpam";
                            actionTypeID = 16;
                        }
                        else if (emailState == "dropped")
                        {
                            triggerType = "EmailFailed";
                            actionTypeID = 17;
                        }
                        //else if (emailState == "deferred")
                        //{
                        //    triggerType = "EmailSoftbounce";
                        //    actionTypeID = 14;
                        //}
                        else if ((emailState == "click"))
                        {
                            actionTypeID = 13;
                            triggerType = "EmailClick";
                            //clickURL = ja[i]["url"].ToString();
                            var url = itemProperties.FirstOrDefault(x => x.Name == "url");
                            if (url != null)
                                clickURL = url.Value.ToString();
                        }

                        if (!string.IsNullOrEmpty(sendUserID))
                        {
                            if (IgnoreClient.IndexOf(sendUserID) < 0) {
                                string constr = Common.GetConnectionStr(int.Parse(sendUserID));
                                if (!string.IsNullOrEmpty(custormId) && !string.IsNullOrEmpty(eventID) && !string.IsNullOrEmpty(triggerType))
                                {
                                    //ChildEvent chd = GetChildEvent(eventID, triggerType, constr, sendUserID, custormId);
                                    //if (chd != null)
                                    //{
                                    //    AddChildEvent(chd.CampaignID, int.Parse(custormId), int.Parse(sendUserID), chd.EventID, chd.EventType, triggerType, constr);
                                    //}
                                    AddChildEvent(int.Parse(custormId), int.Parse(sendUserID), int.Parse(eventID), triggerType, constr);
                                }
                                if (!string.IsNullOrEmpty(custormId) && !string.IsNullOrEmpty(eventID) && actionTypeID != 0)
                                {
                                    AddActionLog(eventID, custormId, sendUserID, actionTypeID, clickURL, timestamp, constr);
                                }
                            }
                            else
                            {
                                logger.Error("Ignored: ClientId " + sendUserID + ", " + ja[i].ToString());
                            }
                        }
                        else
                        {
                            logger.Error("Failed: ClientId is missing, " + ja[i].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on ProcessIncomingEvent: " + ex.Message);
                if (target != null && target.Length > 0)
                {
                    string strTemp = Encoding.UTF8.GetString(target.ToArray());
                    logger.Error("Error info:" + strTemp);
                }
            }
        }

        private bool IsJsonContent(HttpContent content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            if (content.Headers == null || content.Headers.ContentType == null)
            {
                return false;
            }

            MediaTypeHeaderValue contentType = content.Headers.ContentType;
            return
                contentType.MediaType.Equals("application/json", StringComparison.OrdinalIgnoreCase) ||
                contentType.MediaType.Equals("text/json", StringComparison.OrdinalIgnoreCase);
        }

        private ChildEvent GetChildEvent(string eventId, string triggerType, string constr, string clientId, string customerId)
        {
            try
            {
                string sql = string.Format("Select EventID,EventType,CampaignID,ActivityID,ParentEventID,TriggerType from Event with(nolock) Where ParentEventID = {0} and TriggerType = '{1}'", eventId, triggerType);
                DbHelperSQL dbh = new DbHelperSQL(constr);
                DataTable dt = dbh.Query(sql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    ChildEvent ce = new ChildEvent();
                    ce.EventID = (int)dt.Rows[0]["EventID"];
                    ce.EventType = (int)dt.Rows[0]["EventType"];
                    ce.CampaignID = (int)dt.Rows[0]["CampaignID"];
                    ce.ActivityID = (int)dt.Rows[0]["ActivityID"];
                    ce.ParentEventID = (int)dt.Rows[0]["ParentEventID"];
                    ce.TriggerType = dt.Rows[0]["TriggerType"].ToString();
                    return ce;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on GetChildEvent : " + ex.Message);
                logger.Error(string.Format("Info: ClientId={0}, EventID={1}, TriggerType={2}, CustomerId={3}", clientId, eventId, triggerType, customerId));
                logger.Trace(string.Format("GetChildEvent: {0},{1},{2},{3} ", clientId, eventId, triggerType, customerId));
                return null;
            }
        }

        private void AddChildEvent(int CustomerID, int ClientID, int CompletedEventID, string CompletedTriggerType, string constr)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into InboundTable(");
                strSql.Append("CampaignID,CustomerID,ClientId,CompletedEventID,CompletedEventTypeID,CompletedTriggerType)");
                strSql.Append("Select CampaignID,@CustomerID,@ClientId,@EventID,1,@CompletedTriggerType from Event with(nolock) Where ParentEventID=@EventID and TriggerType=@CompletedTriggerType");
                SqlParameter[] parameters = {
                    new SqlParameter("@CustomerID", SqlDbType.Int,4),
                    new SqlParameter("@ClientId", SqlDbType.Int,4),
                    new SqlParameter("@EventID", SqlDbType.Int,4),
                    new SqlParameter("@CompletedTriggerType", SqlDbType.VarChar, 100)};
                parameters[0].Value = CustomerID;
                parameters[1].Value = ClientID;
                parameters[2].Value = CompletedEventID;
                parameters[3].Value = CompletedTriggerType;

                if (!string.IsNullOrEmpty(constr))
                {
                    DbHelperSQL dbh = new DbHelperSQL(constr);
                    object obj = dbh.ExecuteSql(strSql.ToString(), parameters);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on AddChildEvent : " + ex.Message);
                logger.Error(string.Format("Info: Client={0}, EventID={1}, CsId={2}, EventType={3}, TriggerType={4} "
                    , ClientID, CompletedEventID, CustomerID, 1, CompletedTriggerType));
                logger.Trace(string.Format("AddChildEvent: {0},{1},{2},{3},{4} "
                    , ClientID, CompletedEventID, CustomerID, 1, CompletedTriggerType));
            }
        }

        private void AddActionLog(string eventID, string customerId, string sendUserID, int actionTypeID,string clickURL, string timestamp, string constr)
        {
            try
            {
                string sqlActionLog = "";
                if (!string.IsNullOrEmpty(clickURL))
                {
                    IDictionary<string, string> map = new Dictionary<string, string>()
                    {
                       {"%C3%86", "Æ"},{"%C3%A6", "æ"},{"%C3%98", "Ø"},{"%C3%B8", "ø"}, {"%C3%85", "Å"}, {"%C3%A5", "å"},
                       {"%c3%86", "Æ"},{"%c3%a6", "æ"},{"%c3%98", "Ø"},{"%c3%b8", "ø"}, {"%c3%85", "Å"}, {"%c3%a5", "å"}
                    };
                    var regex = new Regex(String.Join("|", map.Keys));
                    var newStr = regex.Replace(clickURL, m => map[m.Value]);
                    clickURL = newStr;
                }

                sqlActionLog = "INSERT INTO ActionLog_Email ([CustomerID],[ActivityTypeID],[ActionTypeID],[CampaignID],[EventID],[ReportTag],[url],[Timestamp],[TemplateID]) "
                              + " SELECT " + customerId + ",1," + actionTypeID + ",CampaignID," + eventID + ",'report','" + clickURL + "','" + timestamp + "',ActivityID FROM Event with(nolock) WHERE EventID=" + eventID;

                DbHelperSQL dbh = new DbHelperSQL(constr);
                dbh.ExecuteSql(sqlActionLog);
            }
            catch (Exception ex)
            {
                logger.Error("Error on AddActionLog : " + ex.Message);
                logger.Error(string.Format("Info: Client={0}, ActionTypeID={1}, EventID={2}, CsId={3}, Url={4}, Timestamp={5} "
                    , sendUserID, actionTypeID, eventID, customerId, clickURL, timestamp));
                logger.Trace(string.Format("AddActionLog: {0},{1},{2},{3},{4},{5} "
                    , sendUserID, actionTypeID, eventID, customerId, clickURL, timestamp));
            }
        }

        //public void Post([FromBody]SendGridSentEvent[] eventList)
        //{
        //    if (eventList.Length > 0)
        //    {
        //        List<SendGridEvent> sgEvents = eventList.Select(s => new SendGridEvent(s)).ToList();
        //        foreach (SendGridEvent item in sgEvents)
        //        {
        //            logger.Info(item.ToHtmlString());
        //        }

        //    }
        //}
    }
}
