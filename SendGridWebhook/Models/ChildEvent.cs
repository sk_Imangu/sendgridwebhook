﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendGridWebhook.Models
{
    public class ChildEvent
    {
        public int EventID { get; set; }
        public int EventType { get; set; }
        public int ParentEventID { get; set; }
        public int ActivityID { get; set; }
        public int CampaignID { get; set; }
        public string TriggerType { get; set; }
    }
}