﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendGridWebhook.Models
{
    public class Lead
    {
        public string Forename { get; set; }
        public string Lastname { get; set; }
        public string Telephone1 { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }

    public class AddLeadUsingSource
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int ClientId { get; set; }
        public int SourceID { get; set; }
        public string Origin { get; set; }
        public string Forename { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Telephone4 { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string CustomerNr { get; set; }
        public string Companyname { get; set; }
        public string Gender { get; set; }
        public string CustomerStatus { get; set; }
        public string PersonID { get; set; }
        public string ExtFields { get; set; }
        public string HeroOutboundID { get; set; }
        public int? TMCampaignID { get; set; }
    }
}