﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendGridWebhook.Models
{
    public class EmailEvent
    {
        public string timestamp { get; set; }
        public string clientId { get; set; }
        public string customerId { get; set; }
        public string eventId { get; set; }
        public string url { get; set; }
        public string sendgridEvent { get; set; }
        public string sendgridReason { get; set; }
        public string sendgridIP { get; set; }
        public string sendgridUserAgent { get; set; }
    }
}