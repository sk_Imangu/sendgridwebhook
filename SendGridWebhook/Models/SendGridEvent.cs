﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendGridWebhook.Models
{
    public class SendGridEvent
    {
        public SendGridEvent()
        {

        }

        public SendGridEvent(SendGridSentEvent sentEvent)
        {
            Event = sentEvent.@event;
            EmailAddress = sentEvent.email;
            Category = sentEvent.category;
            Response = sentEvent.response;
            Attempt = sentEvent.attempt;
            EventDate = TimeStampToDateTime(Convert.ToDouble(sentEvent.timestamp));
            Url = sentEvent.url;
            Status = sentEvent.status;
            Reason = sentEvent.reason;
            Type = sentEvent.type;
        }

        public int SendGridEventID { get; set; }
        public string Event { get; set; }
        public string EmailAddress { get; set; }
        public string Category { get; set; }
        public string Response { get; set; }
        public string Attempt { get; set; }
        public DateTime EventDate { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string Type { get; set; }

        public override string ToString()
        {
            string ret = "Event : " + Event + "\n";
            ret += "EmailAddress : " + EmailAddress + "\n";
            ret += "Category : " + Category + "\n";
            ret += "Response : " + Response + "\n";
            ret += "Attempt : " + Attempt + "\n";
            ret += "EventDate : " + EventDate.ToString() + "\n";
            ret += "Url : " + Url + "\n";
            ret += "Status : " + Status + "\n";
            ret += "Reason : " + Reason + "\n";
            ret += "Type : " + Type + "\n";
            return ret;
        }

        public string ToHtmlString()
        {
            string ret = this.ToString().Replace("\n", "<br />\n");
            return ret;
        }

        private DateTime TimeStampToDateTime(double timeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(timeStamp).ToLocalTime();
            return dateTime;
        }
    }
}