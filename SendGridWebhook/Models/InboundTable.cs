﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendGridWebhook.Models
{
    public class InboundTable
    {
        public InboundTable()
        { }

        private int _inboundEventID;
        private int _campaignID;
        private int _customerID;
        private int _clientID;
        /// <summary>
        /// 
        /// </summary>
        public int InboundEventID
        {
            set { _inboundEventID = value; }
            get { return _inboundEventID; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CampaignID
        {
            set { _campaignID = value; }
            get { return _campaignID; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CustomerID
        {
            set { _customerID = value; }
            get { return _customerID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ClientID
        {
            set { _clientID = value; }
            get { return _clientID; }
        }

        public int? CompletedEventID { get; set; }
        public int? CompletedEventTypeID { get; set; }
        public string CompletedTriggerType { get; set; }
    }
}