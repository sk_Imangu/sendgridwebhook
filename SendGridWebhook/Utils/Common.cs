﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using NLog;
using System.Xml;

namespace SendGridWebhook.Utils
{
    public sealed class Common
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static Dictionary<int, string> ClientList = new Dictionary<int, string>();
        private static string ClientDBConfig = System.Configuration.ConfigurationManager.AppSettings["ClientDBConfig"].ToString();
        private static string DefaultClientConn = ConfigurationManager.ConnectionStrings["ConnectionTemplate"].ConnectionString;
        private static string IvodbConn = ConfigurationManager.ConnectionStrings["IvoDB"].ConnectionString;

        public static string GetConnectionStr(int clientId)
        {
            try
            {
                string dbName = "";
                if (ClientList.Keys.Contains(clientId))
                {
                    dbName = ClientList[clientId];
                }
                else
                {
                    DbHelperSQL DBH = new DbHelperSQL(IvodbConn);
                    DataTable dt = DBH.Query("SELECT uid, dbName FROM dbo.UserToDB uid=" + clientId).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        dbName = dt.Rows[0]["dbName"].ToString();
                        ClientList.Add(clientId, dbName);
                    }
                }
                if (!string.IsNullOrEmpty(dbName))
                {
                    string xmlfilepath = ClientDBConfig;
                    string constr = string.Empty;
                    string clntstr = "";
                    string wherestr = string.Format("clients/client[@dbname='{0}']", dbName);
                    XmlNodeList nodelist = XmlNodeTolist(xmlfilepath, "clients", wherestr);

                    foreach (XmlNode node in nodelist)
                    {
                        clntstr = node.Attributes["connection"].Value;
                    }
                    string xmlstr = DefaultClientConn;
                    if (!string.IsNullOrEmpty(clntstr))
                        xmlstr = clntstr;
                    if (!string.IsNullOrEmpty(xmlstr))
                        constr = string.Format(xmlstr, dbName);
                    else
                        constr = DefaultClientConn;

                    return string.Format(constr, dbName);
                }
                else
                {
                    logger.Error("Failed on GetConnectionStr: Cannot get dbname of client " + clientId);
                    return "";
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on GetConnectionStr: " + ex.Message);
                return "";
            }
        }

        public static List<T> TableToList<T>(DataTable table) where T : class,new()
        {
            List<T> data = new List<T>();
            foreach (DataRow row in table.Rows)
            {
                T obj = new T();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType));
                    }
                    catch
                    {
                        continue;
                    }
                }
                data.Add(obj);
            }
            return data;
        }

        public static void GetClientDBList()
        {
            try
            {
                DbHelperSQL DBH = new DbHelperSQL(IvodbConn);
                DataSet DS = DBH.Query("SELECT uid, dbName FROM dbo.UserToDB ");
                foreach (DataRow row in DS.Tables[0].Rows)
                {
                    int clntId = (int)row["uid"];
                    if (!ClientList.ContainsKey(clntId))
                        ClientList.Add(clntId, row["dbName"].ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on GetClientDBList: " + ex.Message);
            }
        }

        public static string ConvertUnixTimestamp(string input)
        {
            try
            {
                long unixDateTime = 0;
                if (!string.IsNullOrEmpty(input) && long.TryParse(input, out unixDateTime))
                {
                    var timeInTicks = unixDateTime * TimeSpan.TicksPerSecond;
                    var inputDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddTicks(timeInTicks).ToUniversalTime();

                    TimeZoneInfo dsh = TimeZoneInfo.FindSystemTimeZoneById("Romance Standard Time");
                    DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(inputDateTime, dsh);
                    return localDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    logger.Error("Parse date failed: Used current date.");
                    return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error on ConvertUnixTimestamp :" + ex.Message);
                return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public static string GetConfigString(string key)
        {
            object objModel = new object();
            try
            {
                objModel = ConfigurationManager.AppSettings[key];
            }
            catch { }

            return objModel.ToString();
        }

        private static XmlNodeList XmlNodeTolist(string xmlpath, string parentNodeName, string wherestr)
        {
            XmlNodeList chilenodeList = null;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlpath);
                chilenodeList = xmlDoc.SelectNodes(wherestr);

            }
            catch (Exception e)
            {
                throw new Exception("Not parse the XML document format or an XML syntax error", e);
            }

            return chilenodeList;
        }
    }
}